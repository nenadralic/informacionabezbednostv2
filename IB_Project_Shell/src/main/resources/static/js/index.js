$(document).ready(function(){
	$('#loginSubmit').on('click', function(event) {	
		event.preventDefault();
		var usernameInput = $('#usernameInput');
		var passwordInput = $('#passwordInput');
		
		var username = usernameInput.val();
		var password = passwordInput.val();
		
		if($('#usernameInput').val() == "" || $('#passwordInput').val() == ""){
            alert('Niste uneli sve potrebne informacije!');
            return;
        }
		$.post('api/users/user/login', {'username': username, 'password': password},
			function(response){
				window.location.href = 'glavna.html';
		}).fail(function(){
			console.log("Gde si posao!?")	
		});
	});
});