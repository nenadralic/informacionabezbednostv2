$(document).ready(function(){
	$('#registrationSubmit').on('click', function(event) {
		event.preventDefault();
		var usernameInput = $('#usernameInput');
		var passwordInput = $('#passwordInput');
		
		var username = usernameInput.val();
		var password = passwordInput.val();
		
		if($('#usernameInput').val() == "" || $('#passwordInput').val() == "" ){
            alert('Niste popunili sva polja!');
            return;      
        }
		$.post('api/users/user/registration', {'username': username, 'password': password},
			function(response){
				window.location.href = 'index.html';
		}).fail(function(){
			console.log(password)
			console.log("Dalje neces moci")
		});
	});
});