DROP SCHEMA IF EXISTS ib;
CREATE SCHEMA ib DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE ib;

CREATE TABLE user(
	id INT AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL, 
	authority VARCHAR(10) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO user (id, username, password, authority) VALUES (1, 'radeee14','123', 'Admin');
INSERT INTO user (id, username, password,  authority) VALUES (2, 'zureee14','123', 'Regular');
