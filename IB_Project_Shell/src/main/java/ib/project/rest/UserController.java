package ib.project.rest;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import ib.project.model.User;
import ib.project.service.UserService;


@RestController
@RequestMapping(value="api/users")
public class UserController {

	@Autowired
	public UserService userService;
	
	@GetMapping(path="/")
	public ArrayList<User> findAll() {
		System.out.println(userService.findAll());
		return userService.findAll();
	}
	
	@GetMapping(path="user/email")
	public ResponseEntity<User> userEmail(@RequestParam String username) {
		User user = userService.findByUsername(username);
		if (user != null) {
			return new ResponseEntity<User>(user,HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@PostMapping(path="user/login")
	public ResponseEntity<User> loginUser(@RequestParam String username, @RequestParam String password) {
		User user = userService.findByUsernameAndPassword(username, password);
		try {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(path="user/registration")
	public ResponseEntity<User> registrationUser(@RequestParam String username, @RequestParam String password) {
		User user = new User();
		User checkUser = userService.findByUsername(username);
		if (checkUser == null) {
			user.setAuthority("Regular");
			user.setUsername(username);
			user.setPassword(password);
			
			userService.save(user);
			return new ResponseEntity<User>(user,HttpStatus.CREATED);
		}else {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}
	
	
}
