package ib.project.service;

import java.util.List;

import ib.project.model.User;


public interface UserServiccInterface {

	public User findOne(Long id);

	public User findByUsername(String username);
	
	public List<User> findAll();

	public User save(User user);

	public void remove(Long id);

	public User findByUsernameAndPassword(String username, String password);

}
