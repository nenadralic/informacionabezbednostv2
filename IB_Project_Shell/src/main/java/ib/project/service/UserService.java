package ib.project.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ib.project.model.User;
import ib.project.repository.UserRepository;



@Service
public class UserService implements UserServiccInterface {

	@Autowired
	UserRepository userRepository;
	
	@Override
	public ArrayList<User> findAll() {
		return (ArrayList<User>) userRepository.findAll();
	}
	
	@Override
	public User findByUsername(String email) {
		return userRepository.findByUsername(email);
	}


	public User save(User user) {
		return userRepository.save(user);
	}


	@Override
	public User findByUsernameAndPassword(String username, String password) {
		return userRepository.findByUsernameAndPassword(username, password);
	}

	@Override
	public User findOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		
	}
}
