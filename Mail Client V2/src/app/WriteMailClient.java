package app;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.io.FileInputStream;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.mail.internet.MimeMessage;

import org.apache.xml.security.utils.JavaUtils;

import com.google.api.services.gmail.Gmail;

import crypto.AsymmetricKeyEncryption;
import model.mailclient.MailBody;
import signature.SignEnveloped;
import util.Base64;
import util.GzipUtil;
import util.IVHelper;
import support.MailHelper;
import support.MailWritter;
import util.KeyStoreReader;
import xml.CreateXmlDOM;

public class WriteMailClient extends MailClient {

//	private static final String KEY_FILE = "./data/session.key";
//	private static final String IV1_FILE = "./data/iv1.bin";
//	private static final String IV2_FILE = "./data/iv2.bin";
	
	public static void main(String[] args) {
		
        try {
        	Gmail service = getGmailService();
            
        	
        	System.out.println("Insert your email:");
        	BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
            String sender = bufferReader.readLine();
            final String xmlFilePath = "./data/" + sender + "_enc.xml";
        	
        	System.out.println("Insert a reciever:");
            String reciever = bufferReader.readLine();
        	
            System.out.println("Insert a subject:");
            String subject = bufferReader.readLine();
            
            
            System.out.println("Insert body:");
            String body = bufferReader.readLine();
            
            
            CreateXmlDOM.createXML(sender, subject, body);
            
            
        	SignEnveloped.testIt(sender);
        	
        	
            AsymmetricKeyEncryption.testIt(sender, reciever);
            
            MimeMessage mimeMessage = MailHelper.createMimeMessage(reciever, xmlFilePath);
            MailWritter.sendMessage(service, "me", mimeMessage);
            
 
//++++++++++++++++++++++++++++++++++++KONTROLNA TACKA++++++++++++++++++++++++++++++++++++++++++++++++++
            //Compression
//            String compressedSubject = Base64.encodeToString(GzipUtil.compress(subject));
//            String compressedBody = Base64.encodeToString(GzipUtil.compress(body));
//            
//            //Key generation
//            KeyGenerator keyGen = KeyGenerator.getInstance("AES"); 
//			SecretKey secretKey = keyGen.generateKey();
//			Cipher aesCipherEnc = Cipher.getInstance("AES/CBC/PKCS5Padding");
//			
//			//inicijalizacija za sifrovanje 
//			IvParameterSpec ivParameterSpec1 = IVHelper.createIV();
//			aesCipherEnc.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec1);
//			
//			
//			//sifrovanje
//			byte[] ciphertext = aesCipherEnc.doFinal(compressedBody.getBytes());
//			String ciphertextStr = Base64.encodeToString(ciphertext);
//			System.out.println("Kriptovan tekst: " + ciphertextStr);
//			
//			
//			//inicijalizacija za sifrovanje 
//			IvParameterSpec ivParameterSpec2 = IVHelper.createIV();
//			aesCipherEnc.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec2);
//			
//			byte[] ciphersubject = aesCipherEnc.doFinal(compressedSubject.getBytes());
//			String ciphersubjectStr = Base64.encodeToString(ciphersubject);
//			System.out.println("Kriptovan subject: " + ciphersubjectStr);
//			
//			KeyStoreReader keyStoreReader = new KeyStoreReader();
//			
//			keyStoreReader.load(new FileInputStream("./data/usera.jks"), "cevizavodu14");
//			Certificate certificatUserB = keyStoreReader.getCertificate("userb");
//			PublicKey publicKey = certificatUserB.getPublicKey();
//			Cipher rsaCipherEncrypted = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//			rsaCipherEncrypted.init(Cipher.ENCRYPT_MODE, publicKey);
//			byte[] cryptedKey = rsaCipherEncrypted.doFinal(secretKey.getEncoded());
//			System.out.println("Kriptovan kljuc: " + Base64.encodeToString(cryptedKey));
//			
//			MailBody  mailBody = new MailBody(ciphertextStr, ivParameterSpec1.getIV(), ivParameterSpec2.getIV(), cryptedKey);
//
//
//			
//			//snimaju se bajtovi kljuca i IV.
//			JavaUtils.writeBytesToFilename(KEY_FILE, secretKey.getEncoded());
//			JavaUtils.writeBytesToFilename(IV1_FILE, ivParameterSpec1.getIV());
//			JavaUtils.writeBytesToFilename(IV2_FILE, ivParameterSpec2.getIV());
//			
//        	MimeMessage mimeMessage = MailHelper.createMimeMessage(reciever, ciphersubjectStr, mailBody.toCSV());
//        	MailWritter.sendMessage(service, "me", mimeMessage);
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            
        }catch (Exception e) {
        	e.printStackTrace();
		}
	}
}
