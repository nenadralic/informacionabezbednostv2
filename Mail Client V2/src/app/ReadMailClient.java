package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.io.FileInputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.apache.xml.security.utils.JavaUtils;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import org.w3c.dom.Document;

import crypto.AsymmetricKeyDecryption;
import crypto.AsymmetricKeyEncryption;
import support.MailHelper;
import support.MailReader;
import util.Base64;
import util.GzipUtil;
import util.KeyStoreReader;
import xml.CreateXmlDOM;
import model.mailclient.MailBody;
import signature.VerifySignatureEnveloped;

public class ReadMailClient extends MailClient {

	public static long PAGE_SIZE = 3;
	public static boolean ONLY_FIRST_PAGE = true;
		
	public static void main(String[] args) throws IOException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException, MessagingException, NoSuchPaddingException, InvalidAlgorithmParameterException {
        // Build a new authorized API client service.
        Gmail service = getGmailService();
        ArrayList<MimeMessage> mimeMessages = new ArrayList<MimeMessage>();
        
        String user = "me";
        String query = "is:unread label:INBOX";
        String sender = "";
        String reciever = "";
        
        List<Message> messages = MailReader.listMessagesMatchingQuery(service, user, query, PAGE_SIZE, ONLY_FIRST_PAGE);
        for(int i=0; i<messages.size(); i++) {
        	Message fullM = MailReader.getMessage(service, user, messages.get(i).getId());
        	
        	MimeMessage mimeMessage;
			try {
				
				mimeMessage = MailReader.getMimeMessage(service, user, fullM.getId());
				
				sender = mimeMessage.getHeader("From", null);
				reciever = mimeMessage.getHeader("To", null);
				
				System.out.println("\n Message number " + i);
				System.out.println("From: " + mimeMessage.getHeader("From", null));
				System.out.println("Subject: " + mimeMessage.getSubject());
				System.out.println("Body: " + MailHelper.getText(mimeMessage));
				System.out.println("\n");
				
				mimeMessages.add(mimeMessage);
	        
			} catch (MessagingException e) {
				e.printStackTrace();
			}	
        }
        
        System.out.println("Select a message to decrypt:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	        
	    String answerStr = reader.readLine();
	    Integer answer = Integer.parseInt(answerStr);
	    
		MimeMessage chosenMessage = mimeMessages.get(answer);
		
		
		AsymmetricKeyDecryption.testIt(sender, reciever);
		
		
		VerifySignatureEnveloped.testIt(sender);
		
		
		System.out.println("============= Message =============");
		System.out.println("From: " + sender);
		Document doc = AsymmetricKeyEncryption.loadDocument("./data/" + sender + "_dec.xml");
		CreateXmlDOM.printMail(doc);
		
//+++++++++++++++++++++++++++++++++++++++++++++++KONTROLNA TACKA++++++++++++++++++++++++++++++++++++++++++++++++++++++		
//		MailBody mailBody = new MailBody(MailHelper.getText(chosenMessage));
//
//		KeyStoreReader keyStoreReader;
//		byte[] secretKeyBytes = null;
//		try {
//			keyStoreReader = new KeyStoreReader();
//			// Sifra je jednostavna kako ne bih morao da je pamtim, shvatam da nije za
//			// upotrebu u stvarnom svetu
//			keyStoreReader.load(new FileInputStream("./data/userb.jks"), "cevizavodu14");
//			PrivateKey privateKey = keyStoreReader.getKey("userb", "cevizavodu14");
//			Cipher rsaCipherDec = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//			rsaCipherDec.init(Cipher.DECRYPT_MODE, privateKey);
//
//			secretKeyBytes = rsaCipherDec.doFinal(mailBody.getEncKeyBytes());
//			for (byte b : secretKeyBytes)
//				System.out.print(b);
//			System.out.println("\n");
//
//		} catch (KeyStoreException | NoSuchProviderException | CertificateException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	    
//        //TODO: Decrypt a message and decompress it. The private key is stored in a file.
//		Cipher aesCipherDec = Cipher.getInstance("AES/CBC/PKCS5Padding");
//		SecretKey secretKey = new SecretKeySpec(JavaUtils.getBytesFromFile(KEY_FILE), "AES");
//		
//		
//		byte[] iv1 = JavaUtils.getBytesFromFile(IV1_FILE);
//		IvParameterSpec ivParameterSpec1 = new IvParameterSpec(iv1);
//		aesCipherDec.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec1);
//		
//		String str = MailHelper.getText(chosenMessage);
//		byte[] bodyEnc = Base64.decode(str);
//		
//		String receivedBodyTxt = new String(aesCipherDec.doFinal(bodyEnc));
//		String decompressedBodyText = GzipUtil.decompress(Base64.decode(receivedBodyTxt));
//		System.out.println("Body text: " + decompressedBodyText);
//		
//		
//		byte[] iv2 = JavaUtils.getBytesFromFile(IV2_FILE);
//		IvParameterSpec ivParameterSpec2 = new IvParameterSpec(iv2);
//		//inicijalizacija za dekriptovanje
//		aesCipherDec.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec2);
//		
//		//dekompresovanje i dekriptovanje subject-a
//		String decryptedSubjectTxt = new String(aesCipherDec.doFinal(Base64.decode(chosenMessage.getSubject())));
//		String decompressedSubjectTxt = GzipUtil.decompress(Base64.decode(decryptedSubjectTxt));
//		System.out.println("Subject text: " + new String(decompressedSubjectTxt));
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		
	}
}
