package support;
import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailHelper {

    public static String getText(Part part) throws
            MessagingException, IOException {
        if (part.isMimeType("text/*")) {
            String stringContent = (String) part.getContent();
            return stringContent;
        }
        if (part.isMimeType("multipart/alternative")) {
            Multipart multiPart = (Multipart) part.getContent();
            String text = null;
            for (int i = 0; i < multiPart.getCount(); i++) {
                Part bodyPart = multiPart.getBodyPart(i);
                if (bodyPart.isMimeType("text/plain")) {
                    if (text == null) {
                        text = getText(bodyPart); 
                    }
                    continue;
                } else if (bodyPart.isMimeType("text/html")) {
                    String stringBodyPart = getText(bodyPart);
                    if (stringBodyPart != null) {
                        return stringBodyPart; 
                    }   
                } else {
                    return getText(bodyPart);      
                }
            }
            return text;
        } else if (part.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) part.getContent();
            for (int i = 0; i < mp.getCount(); i++) {   	
                String s = getText(mp.getBodyPart(i));
                if (s != null) {	
                    return s;           
                }
            }
        }
        return null;
    }

    public static MimeMessage createMimeMessage(String reciever, String fileName) throws MessagingException {
    	
    	Properties property = new Properties();
	    Session session = Session.getDefaultInstance(property, null);
    	MimeMessage message = new MimeMessage(session);

    	BodyPart messageBodyPart1 = new MimeBodyPart();  
        messageBodyPart1.setText("This message is encrypted");
    	
    	BodyPart messageBodyPart2 = new MimeBodyPart();
    	DataSource source = new FileDataSource(fileName);
    	messageBodyPart2.setDataHandler(new DataHandler(source));
    	messageBodyPart2.setFileName(fileName);
    	
    	Multipart multiPart = new MimeMultipart();
    	multiPart.addBodyPart(messageBodyPart1);
    	multiPart.addBodyPart(messageBodyPart2);
    	
    	message.setSubject("Encrypted message");
    	message.setRecipient(Message.RecipientType.TO, new InternetAddress(reciever));
    	message.setContent(multiPart);
    	
    	return message;
    	
    }
}
